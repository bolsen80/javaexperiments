# Java Experiments

This is just a repo of random classes to test things in JAVA that I wanted to give a try.

## So far:
 * Using gradle
 * Basic usage of JDBC/PostgreSQL module (`org.bolsen.db`)
 * Implement a simple UDP server/client using Netty as a test-base for a project idea (`org.bolsen.net`)
 * Setup a simple embedded Jetty server, implementing an API that does mostly useful, except to see how to do servlet-type things (`org.bolsen.org.bolsen.web`)
 * HttpServlet that spits out JSON using Jackson's ObjectMapper (`org.bolsen.org.bolsen.web`)
