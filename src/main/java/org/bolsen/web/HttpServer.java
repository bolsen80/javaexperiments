package org.bolsen.web;

import org.eclipse.jetty.server.CustomRequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.Slf4jRequestLogWriter;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

public class HttpServer {
    public static void main(String args[]) {
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setName("server");
        Server server = new Server(threadPool);
        CustomRequestLog logger = new CustomRequestLog(new Slf4jRequestLogWriter(), CustomRequestLog.EXTENDED_NCSA_FORMAT);
        server.setRequestLog(logger);

        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8080);
        server.addConnector(connector);

        // Routing
        ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();

        ContextHandler contextHandler = new ContextHandler();
        contextHandler.setContextPath("/test");
        contextHandler.setHandler(new TestServlet());
        contextHandlerCollection.addHandler(contextHandler);

        ContextHandler dbContextHandler = new ContextHandler();
        dbContextHandler.setContextPath("/db");
        dbContextHandler.setHandler(new DbServlet());
        contextHandlerCollection.addHandler(dbContextHandler);

        server.setHandler(contextHandlerCollection);

        try {
            server.start();
        } catch (Exception e) {
            System.getLogger("logger").log(System.Logger.Level.WARNING, "Failed to start server");
        }
    }
 }
