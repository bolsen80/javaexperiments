package org.bolsen.web;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import java.io.IOException;

public class TestServlet extends AbstractHandler {
    @Override
    public void handle(String target, Request jettyReq, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println("test");
        resp.setContentType("text/html");
        jettyReq.setHandled(true);
    }
}
