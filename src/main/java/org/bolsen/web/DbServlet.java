package org.bolsen.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.bolsen.db.Db;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DbServlet extends AbstractHandler {

    private Db db;

    public DbServlet() {
        super();
    }

    @Override
    public void handle(String target, Request jreq, HttpServletRequest req, HttpServletResponse resp) {

        try {
            this.db = new Db();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            List<String> names = db.execSampleQuery();
            ObjectMapper objectMapper = new ObjectMapper();
            resp.getWriter().println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(names));
            resp.setContentType("text/json");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        jreq.setHandled(true);
    }
}
