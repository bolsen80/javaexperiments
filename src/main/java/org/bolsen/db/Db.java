package org.bolsen.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Db {
    private Connection manager;

    public Db() throws SQLException {
        String postgresUrl = "jdbc:postgresql://localhost:5435/test";
        Properties props = new Properties();
        props.setProperty("user", "admin");
        props.setProperty("password", "test");
        this.manager = DriverManager.getConnection(postgresUrl, props);
    }

    public List<String> execSampleQuery() throws SQLException {
        PreparedStatement st = this.manager.prepareStatement("SELECT * FROM names");
        // st.setString(1, "Brian");
        ResultSet rs = st.executeQuery();
        ArrayList<String> names = new ArrayList<>();
        while (rs.next()) {
            names.add(rs.getString(2));
        }

        rs.close();
        st.close();

        return names;
    }
}
