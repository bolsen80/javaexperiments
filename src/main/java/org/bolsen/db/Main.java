package org.bolsen.db;

import java.sql.*;
import java.util.Properties;

public class Main {
    public Connection manager;

    public static void main(String[] args) throws SQLException {
        Main main = new Main();
        main.execSampleQuery();
    }

    public Main() throws SQLException {
        String postgresUrl = "jdbc:postgresql://localhost:5435/test";
        Properties props = new Properties();
        props.setProperty("user", "admin");
        props.setProperty("password", "test");
        this.manager = DriverManager.getConnection(postgresUrl, props);

        execSampleQuery();
    }

    private void execSampleQuery() throws SQLException {
        PreparedStatement st = this.manager.prepareStatement("SELECT count(*) FROM names");
        // st.setString(1, "Brian");
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            System.out.print("Column 1 returned ");
            System.out.println(rs.getString(1));
        }

        rs.close();
        st.close();
    }

    protected void finalize() throws Throwable {
        this.manager.close();
        System.out.println("finalized");
    }
}